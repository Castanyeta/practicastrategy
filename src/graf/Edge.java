package graf;

public class Edge {


	/** Attributes */
	private Node origin;
	private Node destiny;


	/** Constructors */
	public Edge(Node o,Node d){
		origin = o;
		destiny = d;
	}

	/** Getters */
	public Node getOrigin(){
		return origin;
	}

	public Node getDestiny(){
		return destiny;
	}

	@Override
	public String toString() {
		return origin.getName() + " -> " + destiny.getName();
	}

	@Override
	public int hashCode() {
		return (origin.getName() + destiny.getName()).hashCode();
	}
}
