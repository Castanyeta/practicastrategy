package graf;

import java.util.*;

public class Dijkstra implements OperationBehaviour{

	/**
	 * Private class to control visited, distance and previous Node to make the new Graph with shortest path.
	 *
	 */
	private class DijkstraElement{
		/**
		 * attributtes
		 */
		private boolean visited;
		private float distance;
		private Node previous;

		/**
		 * Constructors
		 */
		public DijkstraElement(){
			this.distance = -1;
			this.visited = false;
			this.previous = null;
		}

		/**
		 * Getters & Setters
		 */
		public boolean isVisited() {
			return visited;
		}

		public void setVisited(boolean visited) {
			this.visited = visited;
		}

		public float getDistance() {
			return distance;
		}

		public void setDistance(float distance) {
			this.distance = distance;
		}

		public Node getPrevious() {
			return previous;
		}

		public void setPrevious(Node node) {
			this.previous = node;
		}
	}

	/** Interface Method */

	/**
	 * Algorithm to find the shortest path of g between o and d
	 * @param g != null
	 * @param o != null
	 * @param d != null
	 * @return new graph with the shortest path
	 */
	@Override
	public Graph operate(Graph g, Node o, Node d) {

		Graph GG = g;
		HashMap<Node, DijkstraElement> elements = new HashMap<>();
		Iterator<Node> nodes = g.getNodes();

		while(nodes.hasNext()){
			Node node = nodes.next();
			elements.put(node, new DijkstraElement());
		}
		ArrayDeque<Node> cuaVertex = new ArrayDeque();

		cuaVertex.add(o);
		elements.get(o).setDistance(0);

		while(!cuaVertex.isEmpty()){

			Node nodeActual = cuaVertex.poll();
			DijkstraElement actual = elements.get(nodeActual);

			actual.setVisited(true);
			ArrayList<Node> neighbours = g.getNeighbours(nodeActual);


			for( int i = 0 ; i < neighbours.size() ; ++i ){
				Node neighbour = neighbours.get(i);
				DijkstraElement destiny = elements.get(neighbour);
			    if( !destiny.isVisited()){

			    	if((destiny.getDistance() + 1 < destiny.getDistance()) || destiny.getDistance()==-1){
						destiny.setDistance(elements.get(nodeActual).getDistance() + 1);
						destiny.setPrevious(nodeActual);
		    	        cuaVertex.add(neighbour);
		    	    } 
			    }
			}
		}  
		
		return MountGraph(elements, d);
	}

	/** Private methods */

	/**
	 * Mount the graph results from path with final destiny.
	 * @param path contains destiny
	 * @param destiny != null
	 * @return new graph with the shortest path.
	 */
	private Graph MountGraph(HashMap<Node, DijkstraElement> path, Node destiny){
		Node tmp = destiny;
		Set nodes = new HashSet<Node>();
		Set edges = new HashSet<Edge>();
		nodes.add(tmp);
		while(path.get(tmp).getPrevious() != null){
			Node previous = path.get(tmp).getPrevious();
			nodes.add(previous);
			edges.add(new Edge(previous, tmp));
			tmp = previous;
		}
		return new Graph(nodes,edges);
	}
}


