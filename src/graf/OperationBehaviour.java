package graf;


public interface OperationBehaviour {

	public Graph operate(Graph g,Node o, Node d);
	
}
