package graf;

import java.util.HashSet;
import java.util.Set;

public class main {


	/** Main program
	 * This program shows how to apply the Strategy pattern in a example with a graph and different possible operations to apply.
	 * @param args Arguments of the program (now true)
	 */
	public static void main(String[] args) {

		// Creating the nodes
		Node a = new Node("A");
		Node b = new Node("B");
		Node c = new Node("C");
		Node d = new Node("D");
		Node e = new Node("E");
		Node f = new Node("F");

		// Assigning the nodes to a new set
		Set<Node> nodes = new HashSet<>();
		nodes.add(a);
		nodes.add(b);
		nodes.add(c);
		nodes.add(d);
		nodes.add(e);
		nodes.add(f);

		// Creating and filling the edges set
		Set<Edge> edges = new HashSet<>();
		edges.add(new Edge(a, b));
		edges.add(new Edge(a, c));
		edges.add(new Edge(c, d));
		edges.add(new Edge(d, b));
		edges.add(new Edge(b, f));
		edges.add(new Edge(d, e));
		edges.add(new Edge(f, e));

		// Create a graph.
		System.out.println("\n--------------------------------------------------------");
		System.out.println("- This is the graph that will be operated");
		System.out.println("--------------------------------------------------------");
		Graph g = new Graph(nodes, edges);
		System.out.print(g);


		// Dijkstra operation behaviour
		System.out.println("\n--------------------------------------------------------");
		System.out.println("- The shortest path Graph from a dijkstra operation");
		System.out.println("--------------------------------------------------------");
		OperationBehaviour behaviour = new Dijkstra();
		Graph result = behaviour.operate(g, a, e);
		System.out.print(result);

		// Backtracking operation behaviour
		System.out.println("\n--------------------------------------------------------");
		System.out.println("- The shortest path Graph from a Backtracking operation");
		System.out.println("--------------------------------------------------------");
		behaviour = new Backtracking();
		result = behaviour.operate(g, a, e);
		System.out.print(result);

	}

}
