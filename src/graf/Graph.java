package graf;



import java.util.*;

public class Graph {


	/** Attributes */
	private Set<Node> nodes;
	private Set<Edge> edges;
	private OperationBehaviour operate;


	/** Constructors */
	public Graph(){
		nodes = new HashSet<Node>();
		edges = new HashSet<Edge>();
		
	}
	
	public Graph(Set<Node> _nodes,Set<Edge> _edges){
		nodes = _nodes;
		edges = _edges;
	}

	public Iterator<Node> getNodes(){
		return nodes.iterator();
	}

	public Iterator<Edge> getEdges(){
		return edges.iterator();
	}

	/** Getters */
	public int getEdgesSize(){
		return edges.size();
	}

	public int getNodesSize(){
		return nodes.size();
	}
	
	public ArrayList<Node> getNeighbours(Node actual){
	    ArrayList<Node> positions = new ArrayList<>();
	    for(Edge e: edges ) {
		    if(e.getOrigin().equals(actual)){
                positions.add(e.getDestiny());
            }
		}
		return positions;
	}


	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Nodes : \n");
		for (Node n: nodes) {
			builder.append(n + "\n");
		}
		builder.append("Edges: \n");
		for(Edge e:edges){
			builder.append(e + "\n");
		}
		return builder.toString();
	}
}
