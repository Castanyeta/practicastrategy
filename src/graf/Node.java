package graf;

public class Node {

	/** Attributes */
	private String name;

	/** Constructors */
	public Node(String n){
		name=n;
	}

	/** Getters & Setters */
	public String getName(){
		return name;
	}

	@Override
	public String toString() {
		return name;
	}

	@Override
	public int hashCode() {
		return name.hashCode();
	}
}
