package graf;

import java.util.*;

public class Backtracking implements OperationBehaviour {


    /** Attributes */
    HashMap<Node, List<Edge>> graph = new HashMap<>();
    HashMap<Node, Boolean> visited = new HashMap<>();
    Graph shortestPath = new Graph();
    List<Edge> path = new ArrayList<>();
    Node destiny;

    /** Interface Method */
    /**
     * Operates the graph to find the shortest path of g from o to d.
     * @param g g != null
     * @param o o != null
     * @param d d != null
     * @return A new graph with the shortest path.
     */
    @Override
    public Graph operate(Graph g, Node o, Node d) {

        destiny = d;

        // Algorithm Initialization
        Iterator<Node> nodes = g.getNodes();
        while (nodes.hasNext()){
            Node n = nodes.next();
            graph.put(n, new ArrayList<Edge>());
            visited.put(n, false);
        }

        Iterator<Edge> edges = g.getEdges();
        while(edges.hasNext()){
            Edge edge = edges.next();
            if(graph.containsKey(edge.getOrigin())){
                graph.get(edge.getOrigin()).add(edge);
            }
        }

        visited.replace(o, true);
        return iOperate(graph.get(o));
    }

    /**
     * Inmersive operation for the backtracking
     * @param edges
     * @return a new graph mounted with the shortest path.
     */
    private Graph iOperate(List<Edge> edges){
        for(Edge e : edges){
            path.add(e);
            if(path.get(path.size()-1).getDestiny().getName().equals(destiny.getName())) {
                if (shortestPath.getNodesSize() == 0 || path.size() < shortestPath.getNodesSize()) {
                    // We have a better path, so has to be changed.
                    shortestPath = mountShortedPathAsGraph();
                }
            }
            else{
                iOperate(graph.get(e.getDestiny()));
            }
            path.remove(path.size()-1);
        }
        return shortestPath;
    }

    /**
     * @return a new Graph with the shortest path.
     */
    private Graph mountShortedPathAsGraph(){
        Set<Node> nodes = new HashSet<>();
        Set<Edge> edges = new HashSet<>();
        nodes.add(path.get(0).getOrigin());
        for (Edge e: path) {
            edges.add(e);
            nodes.add(e.getDestiny());
        }
        return new Graph(nodes, edges);
    }
}
